var value;
var isPrime;

onmessage = function(event) {
	value = parseInt(event.data);
	checkIncrement();
}

function checkPrime(n) {
	if(n===1)
	{
		return false;
	}
	else if(n === 2)
	{
		return true;
	}
	else
	{
		for(var x = 2; x < n; x++)
		{
		  if(n % x === 0)
		  {
			return false;
		  }
		}
		return true;
	}
}

function checkIncrement()
{
	postMessage(value);
	while(true)
	{
		isPrime = checkPrime(value);
		if(isPrime  == false)
		{
			value = value+1;
			postMessage(value);
			continue;
		}
		postMessage("End");
		break;
	}
}