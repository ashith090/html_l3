**Web worker is not allowed by default from local javascript files. so run chrome with following command from the folder where chrome application file is present**

```sh
.\chrome.exe --allow-file-access-from-files
```

**Note** 
* Intenet explorer and Edge seems to have some issue with postMessage function, which is used for pushing output from web worker to main html, so it is preffered to use chrome as i have tested its working with above command.
* keep both html and js file in same directory